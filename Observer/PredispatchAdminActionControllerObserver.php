<?php
/**
 * Prodovo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the prodovo.com license that is
 * available through the world-wide-web at this URL:
 * https://www.prodovo.co.uk/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Prodovo
 * @package     Prodovo_Base
 * @copyright   Copyright (c) 2019 Prodovo (http://www.prodovo.co.uk/)
 * @license     https://www.prodovo.co.uk/LICENSE.txt
 */

namespace Prodovo\Base\Observer;

use Prodovo\Base\Model\FeedFactory;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Prodovo\Base\Helper\AbstractData;

class PredispatchAdminActionControllerObserver implements ObserverInterface
{
    /**
     * @var FeedFactory
     */
    protected $_feedFactory;
    /**
     * @var Session
     */
    protected $_backendAuthSession;
    /**
     * @var AbstractData
     */
    protected $helper;

    public function __construct(
        FeedFactory $feedFactory,
        AbstractData $helper,
        Session $backendAuthSession
    ) {
        $this->_feedFactory = $feedFactory;
        $this->helper = $helper;
        $this->_backendAuthSession = $backendAuthSession;
    }

    public function execute(Observer $observer)
    {
        if ($this->_backendAuthSession->isLoggedIn() &&
            $this->helper->isModuleOutputEnabled('Magento_AdminNotification')) {
            $feedModel = $this->_feedFactory->create();
            $feedModel->checkUpdate();
        }
    }
}
