# Prodovo - Base

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)

## Main Functionalities
This base extension is required for all Prodovo branded modules.

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Prodovo`
 - Enable the module by running `php bin/magento module:enable Prodovo_Base`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require prodovo/module-base`
 - enable the module by running `php bin/magento module:enable Prodovo_Base`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

## Support
If you have any issues while using any of our modules please feel free to get in touch and we will be happy to help resolve them for you. 

http://www.prodovo.co.uk
