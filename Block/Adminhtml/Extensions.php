<?php
/**
 * Prodovo Auto Coupon
 * Copyright (C) 2020  Prodovo
 *
 * This file is part of Prodovo/AutoCoupon.
 *
 * Prodovo/AutoCoupon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Prodovo\Base\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Extensions extends Field
{

    protected $_template = 'Prodovo_Base::modules.phtml';

    private $moduleListProcessor;

    public function __construct(
        Template\Context $context,
        \Prodovo\Base\Model\ModuleListProcessor $moduleListProcessor,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->moduleListProcessor = $moduleListProcessor;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->toHtml();
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->moduleListProcessor->getModuleList();
    }

}
