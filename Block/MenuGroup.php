<?php
/**
 * Prodovo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the prodovo.com license that is
 * available through the world-wide-web at this URL:
 * https://www.prodovo.co.uk/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Prodovo
 * @package     Prodovo_Base
 * @copyright   Copyright (c) 2019 Prodovo (http://www.prodovo.co.uk/)
 * @license     https://www.prodovo.co.uk/LICENSE.txt
 */

namespace Prodovo\Base\Block;

use Magento\Backend\Block\Context;
use Magento\Backend\Model\Auth\Session;
use Magento\Config\Block\System\Config\Form\Fieldset;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Helper\Js;

class MenuGroup extends Fieldset
{
    /**
     * @var ProductMetadataInterface
     */
    private $metadata;

    public function __construct(
        ProductMetadataInterface $metadata,
        Context $context,
        Session $authSession,
        Js $jsHelper,
        array $data = []
    ) {
        parent::__construct($context, $authSession, $jsHelper, $data);
        $this->metadata = $metadata;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        if (version_compare($this->metadata->getVersion(), '1.0.0', '>=')) {
            return parent::render($element);
        }

        return '';
    }
}
