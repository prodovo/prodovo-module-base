<?php
/**
 * Prodovo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the prodovo.com license that is
 * available through the world-wide-web at this URL:
 * https://www.prodovo.co.uk/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Prodovo
 * @package     Prodovo_Base
 * @copyright   Copyright (c) 2019 Prodovo (http://www.prodovo.co.uk/)
 * @license     https://www.prodovo.co.uk/LICENSE.txt
 */

namespace Prodovo\Base\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class NoticeType implements ArrayInterface
{
    const TYPE_ANNOUNCEMENT = 'announcement';
    const TYPE_NEWUPDATE = 'new_update';
    const TYPE_MARKETING = 'marketing';

    public function toOptionArray()
    {
        $options = [];
        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }
        return $options;
    }

    public function toArray()
    {
        return [
            self::TYPE_ANNOUNCEMENT => __('Announcement'),
            self::TYPE_NEWUPDATE => __('New & Updates'),
            self::TYPE_MARKETING => __('Promotions')
        ];
    }
}
